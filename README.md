# largest_number_x

## Improvements
- [ ] Include unit tests, code quality and code security (for e.g. Sonarqube). 
- [ ] Test it against big data (e.g. 500 GB file)  and provision CPU/ Memory accordingly
- [ ] Use EBS volumes to store arbitrarily large file

# Execution 
-  Run a new pipeline and provide the value of TOPX variable, by default it is set to 5.
