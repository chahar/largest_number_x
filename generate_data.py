from random import randint


with open("data.txt", "a") as data:
    for _ in range(10000):
        random_number = str(randint(1, 99999)) + "\n"
        # Writing data to a file
        data.write(random_number)
