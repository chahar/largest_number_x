import numpy as np
from os import getenv

# Read the environment variable TOPX or default it to largest 5 numbers
TOPX = int(getenv('TOPX', 5))


def topx():
    data = np.loadtxt("data.txt", dtype=int)

    # Find index of topX largest numbers
    index = np.argpartition(data, -TOPX)[-TOPX:]

    # Sort the Index in descending order
    index_sorted = index[np.argsort(data[index])][::-1]

    # Get a list of topX largest numbers
    largest_numbers = data[index_sorted]
    print(f"Below are the top {TOPX} largest numbers from data.txt \n {largest_numbers}")


if __name__ == "__main__":
    topx()
